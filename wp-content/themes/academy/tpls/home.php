<!-- Slider Area Start Here -->
<div class="slider-direction-layout1 slider-layout1 overlay-slider">
    <div class="bend niceties preview-1">
        <div id="ensign-nivoslider-1" class="slides">
            <img src="img/slider/slide1-1.jpg" alt="slider" title="#slider-direction-1" />
            <img src="img/slider/slide1-2.jpg" alt="slider" title="#slider-direction-2" />
            <img src="img/slider/slide1-3.jpg" alt="slider" title="#slider-direction-3" />
        </div>
        <div id="slider-direction-1" class="t-cn slider-direction">
            <div class="slider-content s-tb slide-1">
                <div class="title-container s-tb-c title-light">
                    <div class="row">
            			<div class="col-md-6 col-sm-12">
                        	
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="container text-left">
                                <div class="first-line">
                                    <p class="sl-first-txt">DIGITAL MARKETING</p>
                                </div>
                                <div class="second-line">
                                    <p class="uptxt sl-secend-txt">ვალერი ზირაქაშვილი</p>
                                </div>
                                <div class="slider-sub-text third-line">
                                    <ul>
                                        <li>კურსის განმავლობაში მსმენელები შეისწავლიან ციფრული მედია არხების ფუნქციონირების პრინციპებს</li>
                                    </ul>
                                </div>
                                <div class="slider-btn-area forth-line">
                                    <a href="#" class="btn-fill color-blue border-radius-5 uptxt">დარეგისტრირდი</a>
                                </div>
                            </div>
                    	</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Slider Area End Here -->
<!-- Blog Area Start Here -->
<section class="section-space-default-less30 bg-accent overlay-icon-layout2">
    <div class="container zindex-up">
        <div class="section-heading title-black color-dark text-center">
            <h2>მასტერკლასები</h2>
        </div>
        <div class="row">
        
            <div class="col-md-6 col-sm-12">
                <div class="blog-layout1">
                    <div class="item-img">
                        <a href="#"><img src="img/blog/blog1.jpg" alt="blog" class="img-fluid"></a>
                        <div class="item-date">18-19
                        	<span>ივნისი</span>
                        </div>
                    </div>
                    <div class="item-content text-left">
                        <div class="item-title">
                            <h4 class="title-medium color-dark hover-primary">
                                <a href="single-blog.html">Digital Project Management</a>
                            </h4>
                        </div>
                        <div class="item-deccription">
                            <p>თბილისი, ქორთიარდ მარიოტი</p>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-6 col-sm-12">
                <div class="blog-layout1">
                    <div class="item-img">
                        <a href="#"><img src="img/blog/blog2.jpg" alt="blog" class="img-fluid"></a>
                        <div class="item-date">18-19
                        	<span>ივნისი</span>
                        </div>
                    </div>
                    <div class="item-content text-left">
                        <div class="item-title">
                            <h4 class="title-medium color-dark hover-primary">
                                <a href="single-blog.html">Digital Project Management</a>
                            </h4>
                        </div>
                        <div class="item-deccription">
                            <p>თბილისი, ქორთიარდ მარიოტი</p>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <div class="container zindex-up">
        <div class="about-layout1">
            <a href="#" title="Buy Your Ticket" class="btn-fill color-blue border-radius-5 uptxt">ყველა ტრეინინგის ნახვა</a>
        </div>
    </div>
</section>
<!-- Blog Area End Here -->
<!-- Speaker Area Start Here -->
<section class="section-space-default-less54 overlay-icon-layout3 bg-common bg-primary">
    <div class="container zindex-up">
        <div class="section-heading title-black color-light text-center">
            <h2>ტრენერები</h2>
            <p>Dorem ipsum dolor sit. Incidunt laborum beatae earum nihil onsequuntur officia</p>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-6 col-mb-12">
                <div class="speaker-layout1">
                    <div class="item-img">
                        <img src="img/speaker/speaker1.png" alt="speaker" class="img-fluid rounded-circle">
                        <div class="item-social istrners">
                            <ul>
                                <li>
                                    <a href="#" target="_blank" title="linkedin">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item-title">
                        <h5 class="title-medium size-sm color-light hover-yellow">
                            <a class="uptxt boldtxt" href="single-speaker.html">სახელი გვარი</a>
                        </h5>
                        <h5 class="title-medium size-sm color-yellow hover-yellow">Digital Project Management</h5>
                        <h5 class="title-medium size-sm color-light hover-blue">TBC</h5>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-6 col-mb-12">
                <div class="speaker-layout1">
                    <div class="item-img">
                        <img src="img/speaker/speaker2.png" alt="speaker" class="img-fluid rounded-circle">
                        <div class="item-social istrners">
                            <ul>
                                <li>
                                    <a href="#" target="_blank" title="linkedin">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item-title">
                        <h5 class="title-medium size-sm color-light hover-yellow">
                            <a class="uptxt boldtxt" href="single-speaker.html">ვალერი ზირაქაშვილი</a>
                        </h5>
                        <h5 class="title-medium size-sm color-yellow hover-yellow">Digital Marketing</h5>
                        <h5 class="title-medium size-sm color-light hover-blue">Seenet</h5>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-6 col-mb-12">
                <div class="speaker-layout1">
                    <div class="item-img">
                        <img src="img/speaker/speaker3.png" alt="speaker" class="img-fluid rounded-circle">
                        <div class="item-social istrners">
                            <ul>
                                <li>
                                    <a href="#" target="_blank" title="linkedin">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item-title">
                        <h5 class="title-medium size-sm color-light hover-yellow">
                            <a class="uptxt boldtxt" href="single-speaker.html">სახელი გვარი</a>
                        </h5>
                        <h5 class="title-medium size-sm color-yellow hover-yellow">Digital Project Management</h5>
                        <h5 class="title-medium size-sm color-light hover-blue">TBC</h5>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-6 col-mb-12">
                <div class="speaker-layout1">
                    <div class="item-img">
                        <img src="img/speaker/speaker4.png" alt="speaker" class="img-fluid rounded-circle">
                        <div class="item-social istrners">
                            <ul>
                                <li>
                                    <a href="#" target="_blank" title="linkedin">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item-title">
                        <h5 class="title-medium size-sm color-light hover-yellow">
                            <a class="uptxt boldtxt" href="single-speaker.html">სახელი გვარი</a>
                        </h5>
                        <h5 class="title-medium size-sm color-yellow hover-yellow">Digital Project Management</h5>
                        <h5 class="title-medium size-sm color-light hover-blue">TBC</h5>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-6 col-mb-12">
                <div class="speaker-layout1">
                    <div class="item-img">
                        <img src="img/speaker/speaker5.png" alt="speaker" class="img-fluid rounded-circle">
                        <div class="item-social istrners">
                            <ul>
                                <li>
                                    <a href="#" target="_blank" title="linkedin">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item-title">
                        <h5 class="title-medium size-sm color-light hover-yellow">
                            <a class="uptxt boldtxt" href="single-speaker.html">სახელი გვარი</a>
                        </h5>
                        <h5 class="title-medium size-sm color-yellow hover-yellow">Digital Project Management</h5>
                        <h5 class="title-medium size-sm color-light hover-blue">TBC</h5>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-6 col-mb-12">
                <div class="speaker-layout1">
                    <div class="item-img">
                        <img src="img/speaker/speaker6.png" alt="speaker" class="img-fluid rounded-circle">
                        <div class="item-social istrners">
                            <ul>
                                <li>
                                    <a href="#" target="_blank" title="linkedin">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item-title">
                        <h5 class="title-medium size-sm color-light hover-yellow">
                            <a class="uptxt boldtxt" href="single-speaker.html">სახელი გვარი</a>
                        </h5>
                        <h5 class="title-medium size-sm color-yellow hover-yellow">Digital Project Management</h5>
                        <h5 class="title-medium size-sm color-light hover-blue">TBC</h5>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-6 col-mb-12">
                <div class="speaker-layout1">
                    <div class="item-img">
                        <img src="img/speaker/speaker7.png" alt="speaker" class="img-fluid rounded-circle">
                        <div class="item-social istrners">
                            <ul>
                                <li>
                                    <a href="#" target="_blank" title="linkedin">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item-title">
                        <h5 class="title-medium size-sm color-light hover-yellow">
                            <a class="uptxt boldtxt" href="single-speaker.html">სახელი გვარი</a>
                        </h5>
                        <h5 class="title-medium size-sm color-yellow hover-yellow">Digital Project Management</h5>
                        <h5 class="title-medium size-sm color-light hover-blue">TBC</h5>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-6 col-mb-12">
                <div class="speaker-layout1">
                    <div class="item-img">
                        <img src="img/speaker/speaker8.png" alt="speaker" class="img-fluid rounded-circle">
                        <div class="item-social istrners">
                            <ul>
                                <li>
                                    <a href="#" target="_blank" title="linkedin">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item-title">
                        <h5 class="title-medium size-sm color-light hover-yellow">
                            <a class="uptxt boldtxt" href="single-speaker.html">სახელი გვარი</a>
                        </h5>
                        <h5 class="title-medium size-sm color-yellow hover-yellow">Digital Project Management</h5>
                        <h5 class="title-medium size-sm color-light hover-blue">TBC</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Speaker Area End Here -->
<!-- Sponsonrs Area Start Here -->
<section class="section-space-default bg-light">
    <div class="container">
        <div class="section-heading title-black color-dark text-center">
            <h2>პარტნიორები</h2>
        </div>
        <div class="sponsonrs-layout1">
            <div class="row sponsonrs-row-border">
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="sponsonrs-box">
                        <a href="#">
                            <img src="img/brand/brand1.png" alt="brand" class="img-fluid">
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="sponsonrs-box">
                        <a href="#">
                            <img src="img/brand/brand2.png" alt="brand" class="img-fluid">
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="sponsonrs-box">
                        <a href="#">
                            <img src="img/brand/brand3.png" alt="brand" class="img-fluid">
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="sponsonrs-box">
                        <a href="#">
                            <img src="img/brand/brand4.png" alt="brand" class="img-fluid">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Sponsonrs Area End Here -->
<!-- Inne Page Banner Area Start Here -->
<section class="inner-page-banner" style="background-image: url(img/figure/inner-page-figure.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumbs-area">
                    <h1>აკადემიის შესახებ</h1>
                    <ul>
                        <li>
                            <a href="#">მთავარი</a>
                        </li>
                        <li>შესახებ</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inne Page Banner Area End Here -->
<!-- About Us Start Here -->
<section class="section-space-less30">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="about-layout4">
                    <img src="img/about/about-logo.png" alt="logo" class="img-fluid">
                    <p>პირველი პლატფორმა საქართველოში, რომელიც ციფრული ინდუსტრიის ყველაზე ფართო სპექტრს მოიცავს. ჩვენ ვავსებთ ნიშას კონვენციურ სწავლებასა და თანამედროვე ბაზრის მოთხოვნებს შორის. ჩვენი მიზანი ციფრული ინდუსტრიის ახალგაზრდებში პოპულარიზაცია და არსებულ პროფესიონალებში ცოდნის გაღრმავების ინსპირაციაა. თითოეული კურსი წარმატებული სპიკერის პირად პრაქტიკასა და გამოცდილებაზეა აგებული.</p>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="progress-layout3">
                    <div class="media">
                        <div class="item-icon">
                            <i class="fa fa-users" aria-hidden="true"></i>
                        </div>
                        <div class="media-body">
                            <h3>30+ სპიკერი</h3>
                            <p>Dmply dummy text the printin typeseng industry's standard.</p>
                        </div>
                    </div>
                </div>
                <div class="progress-layout3">
                    <div class="media">
                        <div class="item-icon">
                            <i class="fa fa-clone" aria-hidden="true"></i>
                        </div>
                        <div class="media-body">
                            <h3>15+ პარტნიორი</h3>
                            <p>Event dummy text the printin typeseng industry's standard aindustry.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- About Us Area End Here -->
<!-- Why Join Us Start Here -->
            <section>
                <div class="container-fluid">
                    <div class="row no-gutters full-width">
                        <div class="col-lg-6">
                            <img src="img/about/about2.jpg" alt="about" class="img-fluid width-100">
                        </div>
                        <div class="col-lg-6">
                            <div class="overlay-icon-layout5 height-100 d-flex align-items-center bg-accent">
                                <div class="text-left about-section-padding zindex-up">
                                    <h2 class="title-black color-dark">რატო უნდა შემოგვიერთდე?</h2>
                                    <p>დღეს ციფრული სამყარო თავის დაპოზიციონირების მთავარი ინსტრუმენტია. ნებისმიერი ბიზნესისა თუ ინდივიდისთვის წარმატების მისაღწევად აუცილებელია ციფრული ინდუსტრიის სხვადასხვა მიმართულებაში სრულად გათვითცნობიერების საშუალება ჰქონდეს.</p>
<p>კომფორტულ, მსმენელზე მორგებულ გარემოში შეხვდები ადამიანებს, რომლებსაც შენთვის საინტერესო საკითხებზე უამრავი დრო და ენერგია აქვთ დახარჯული. ისინი გამზადებული კლიშეების ნაცვლად საკუთარ ხედვებსა და უნიკალურ მიდგომებს გაგიზიარებენ, რაც საჭირო უნარების თავად განვითარებაში დაგეხმარება.
</p>
                                    <a href="#" title="Register Now" class="btn-fill color-yellow border-radius-5 size-lg margin-t-30">მასტერკლასები</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Why Join Us Area End Here -->
<!-- Sponsonrs Area Start Here -->
<section class="section-space-default bg-light">
    <div class="container">
        <div class="section-heading title-black color-dark text-center">
            <h2>პარტნიორები</h2>
        </div>
        <div class="sponsonrs-layout1">
            <div class="row sponsonrs-row-border">
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="sponsonrs-box">
                        <a href="#">
                            <img src="img/brand/brand1.png" alt="brand" class="img-fluid">
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="sponsonrs-box">
                        <a href="#">
                            <img src="img/brand/brand2.png" alt="brand" class="img-fluid">
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="sponsonrs-box">
                        <a href="#">
                            <img src="img/brand/brand3.png" alt="brand" class="img-fluid">
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="sponsonrs-box">
                        <a href="#">
                            <img src="img/brand/brand4.png" alt="brand" class="img-fluid">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Sponsonrs Area End Here -->

<?php get_header('blog'); ?>
<!-- Inne Page Banner Area Start Here -->
<section class="inner-page-banner" style="background-image: url(<?php bloginfo('template_url'); ?>/img/figure/inner-page-figure-blog.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumbs-area">
                    <h2><?php the_title(); ?></h2>
                    <ul>
                        <li>
                            <a href="<?php echo home_url(); ?>">მთავარი</a>
                        </li>
                        <li>ბლოგი</li>
                    </ul>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inne Page Banner Area End Here -->


<div class="section-space-less30 bg-accent">
  <div class="container">
    <div class="row">
      <?php if(have_posts()) : ?>
      <?php while(have_posts()) : the_post(); ?>

      <div class="col-lg-12">
        <?php the_post_thumbnail('main-post-large'); ?>
        <div class="blog-content-box bg-light">
          <div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
          <h3 class="blog-content-title"><?php the_title(); ?></h3>
          <p class="blog-date">თარიღი: <span><?php echo the_time('j F, Y'); ?></span></p>
          <div class="blog-content">
            <?php the_content(); ?>
          </div>
          <div class="blog-tags">
            <span>ტეგები:</span>
            <?php
              $before = '';
              $seperator = ''; // blank instead of comma
              $after = '';
              the_tags( $before, $seperator, $after );
            ?>
          </div>
        </div>
        <div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="100%" data-numposts="5"></div>
      </div>

      <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </div>
</div>

<?php get_footer(); ?>

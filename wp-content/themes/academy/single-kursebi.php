<?php get_header(); ?>
<?php
    $tbcUrl = [
        'tite'   => get_the_title(),
        'amount' => 1,
        'price'  => $cfs->get('price'),
        'source' => 'ციფრული ინდუსტრიის აკადემია'
    ];
    $installmentUrl = "https://tbccredit.ge/ganvadeba?utm_source=Academy Of Digital Industries&productName".$tbcUrl['tite']
    ."&productAmount=".$tbcUrl['tite'].'-1'
    ."&totalAmount=".$tbcUrl['price'];
    
?>
<!-- Inne Page Banner Area Start Here -->
<section class="inner-page-banner-t" style="background-image: url(<?php bloginfo('template_url'); ?>/img/figure/inner-page-figure-tr.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumbs-area">
                    <h1><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container mch-box-t">
        <div class="row">
            <div class="col-md-2 col-sm-12">
                <div class="breadcrumbs-area mc-header-t mch-one-t">
                  <i class="fa fa-calendar" aria-hidden="true"></i>
                  <p class="uptxt">პერიოდი</p>
                  <p class="mc-header-txt-t"><?php echo $cfs->get('periodi'); ?></p>
                </div>
            </div>
            <div class="col-md-2 col-sm-12">
                <div class="breadcrumbs-area mc-header-t mch-two-t">
                  <i class="fa fa-clock-o" aria-hidden="true"></i>
                  <p class="uptxt">ხანგრძლივობა</p>
                  <p class="mc-header-txt-t"><?php echo $cfs->get('xangrzlioba'); ?></p>
                </div>
            </div>
            <div class="col-md-3 col-sm-12">
                <div class="breadcrumbs-area mc-header-t mch-three-t">
                  <i class="fa fa-briefcase" aria-hidden="true"></i>
                  <p class="uptxt">განრიგი</p>
                  <p class="mc-header-txt-t"><?php echo $cfs->get('ganrigi'); ?></p>
                </div>
            </div>
            <div class="col-md-5 col-sm-12">
                <div class="breadcrumbs-area mc-header-t-t mch-four-t">
                  <p class="uptxt mc-buy-title-t">ღირებულება</p>
                  <p class="mc-sale-t">
                    <?php echo $cfs->get('sale'); ?>
                    <!--<span class="lari"><?php echo $cfs->get('lari'); ?></span>-->
                  </p>
                  <p class="mc-price-t">
                    <?php echo $cfs->get('price'); ?>
                    <span class="lari">E</span>
                  </p>
                  <span class="slider-btn-area forth-line">
                    <a href="<?php echo home_url(); ?>/signup" class="btn-fill color-red border-radius-5 uptxt">რეგისტრაცია</a>
                    <!--<a target="_blank" href="<?php echo $cfs->get('linkitr'); ?>" class="btn-fill color-red border-radius-5 uptxt">რეგისტრაცია</a>-->
                  </span>
                  <span class="slider-btn-area forth-line ganvadeba">
                      <a target="_blank" href="<?php echo $installmentUrl; ?>" class="btn-fill color-blue border-radius-5 uptxt"><img src="<?php bloginfo('template_url'); ?>/img/tbc.jpg" alt="tbcbank"> განვადება</a>
                  </span>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inne Page Banner Area End Here -->

<!-- Area Start Here -->
<section typeof="Course" class="section-space-default-less bg-dark">
  <div class="container">
    <?php if(have_posts()) : ?>
    <?php while(have_posts()) : the_post(); ?>
      <div class="row">
          <div class="col-md-9 col-sm-12 tr-content">
            <?php the_content(); ?>
          </div>
          <div class="col-md-3 col-sm-12 tr-aside">
            <p class="tr-aside-title uptxt">ტრენერები</p>
            <div class="tr-imgs">
              <?php if (class_exists('MultiPostThumbnails')) :
                  MultiPostThumbnails::the_post_thumbnail( get_post_type(),'speaker-one' ); endif;
              ?>
              <p class="uptxt"><?php echo $cfs->get('pirveli_treneri'); ?></p>
            </div>
            <div class="tr-imgs">
              <?php if (class_exists('MultiPostThumbnails')) :
                  MultiPostThumbnails::the_post_thumbnail( get_post_type(),'speaker-two' ); endif;
              ?>
              <p class="uptxt"><?php echo $cfs->get('meore_treneri'); ?></p>
            </div>
          </div>
      </div>
    <?php endwhile; ?>
    <?php endif; ?>
  </div>
  <div class="container mch-box-t">
    <div class="row">
      <div class="col-md-2 col-sm-12">
          <div class="breadcrumbs-area mc-header-t mch-one-t">
            <i class="fa fa-calendar" aria-hidden="true"></i>
            <p class="uptxt">პერიოდი</p>
            <p class="mc-header-txt-t"><?php echo $cfs->get('periodi'); ?></p>
          </div>
      </div>
      <div class="col-md-2 col-sm-12">
          <div class="breadcrumbs-area mc-header-t mch-two-t">
            <i class="fa fa-clock-o" aria-hidden="true"></i>
            <p class="uptxt">ხანგრძლივობა</p>
            <p class="mc-header-txt-t"><?php echo $cfs->get('xangrzlioba'); ?></p>
          </div>
      </div>
      <div class="col-md-3 col-sm-12">
          <div class="breadcrumbs-area mc-header-t mch-three-t">
            <i class="fa fa-briefcase" aria-hidden="true"></i>
            <p class="uptxt">განრიგი</p>
            <p class="mc-header-txt-t"><?php echo $cfs->get('ganrigi'); ?></p>
          </div>
      </div>
      <div class="col-md-5 col-sm-12">
        <div class="breadcrumbs-area mc-header-t-t mch-four-t">
          <p class="uptxt mc-buy-title-t">ღირებულება</p>
          <p class="mc-sale-t">
            <?php echo $cfs->get('sale'); ?>
            <!--<span class="lari"><?php echo $cfs->get('lari'); ?></span>-->
          </p>
          <p class="mc-price-t">
            <?php echo $cfs->get('price'); ?>
            <span class="lari">E</span>
          </p>
          <span class="slider-btn-area forth-line">
              <a href="<?php echo home_url(); ?>/signup" class="btn-fill color-red border-radius-5 uptxt">რეგისტრაცია</a>
              <!--<a target="_blank" href="<?php echo $cfs->get('linkitr'); ?>" class="btn-fill color-red border-radius-5 uptxt">რეგისტრაცია</a>-->
          </span>
          <span class="slider-btn-area forth-line ganvadeba">
              <a target="_blank" href="<?php echo $installmentUrl; ?>" class="btn-fill color-blue border-radius-5 uptxt"><img src="<?php bloginfo('template_url'); ?>/img/tbc.jpg" alt="tbcbank"> განვადება</a>
          </span>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Area End Here -->
<?php get_footer(); ?>

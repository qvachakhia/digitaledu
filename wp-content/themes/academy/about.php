<?php /* Template Name: about */ ?>
<?php get_header(); ?>
<!-- Inne Page Banner Area Start Here -->
<section class="inner-page-banner" style="background-image: url(<?php bloginfo('template_url'); ?>/img/figure/inner-page-figure.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumbs-area">
                    <h2>აკადემიის შესახებ</h2>
                    <ul>
                        <li>
                            <a href="<?php echo home_url(); ?>">მთავარი</a>
                        </li>
                        <li>აკადემიის შესახებ</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inne Page Banner Area End Here -->
<!-- About Us Start Here -->
<section class="section-space-less30 bg-accent">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="about-layout4">
                  <h3 class="title-black color-dark">როგორ ვქმნით საუკეთესო სასწავლო პლატფორმას?</h3>
                  <h2 class="subtitle">აუდიტორიაზე ადაპტირებული განათლება</h2>
                  <p>კარიერული მიზნები თითოეული ადამიანისთვის უნიკალური და ინდივიდუალურია, ამიტომ, ექსპერტების მეთოდოლოგიით შექმნილი, საერთაშორისო მოთხოვნებთან ადაპტირებული, სასწავლო პლატფორმა მოწვეულ აუდიტორიას საშუალებას აძლევს ჰქონდეს სასურველი კარიერა კრეატიული და ციფრული ინდუსტრიის მიმართულებით.</p>
                    <br>
                    <h2 class="subtitle">რეალური ქეისები, რეალური სამუშაო შესაძლებლობები.</h2>
                    <p>აუდიტორიაზე მორგებული საგანმათლებლო სტრუქტურა, მოიცავს ტრენინგებისა და მასტერკლასების ყველაზე ფართო სპექტრს ციფრულ სამყაროში. მოწვეული სპიკერები სტუდენტებს ყოველდღიური სამუშაო პროცესიდან უზიარებენ რეალურ ქეისებსა და ტენდენციებს, რაც კარიერული წინსვლისთვის საჭირო ხედვებისა და უნარების განვითარებაში ეხმარებათ.  თითოეული კურსის საგანმანათლებლო მიდგომა ითვალისწინებს პარტნიორი კომპანიებისგან შეთავაზებებს: ინტერნისა და ექსპერტის პოზიციებზე.</p>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="progress-layout3">
                    <div class="media">
                        <div class="item-icon">
                            <i class="fa fa-user" aria-hidden="true"></i>
                        </div>
                        <div class="media-body">
                            <h3>30+ სპიკერი</h3>
                            <p><b>ჩვენ ვთანამშრომლობთ ციფრულ ინდუსტრიებში აღიარებულ გურუებთან</b></p>
                        </div>
                    </div>
                </div>
                <div class="progress-layout3">
                    <div class="media">
                        <div class="item-icon">
                            <i class="fa fa-users" aria-hidden="true"></i>
                        </div>
                        <div class="media-body">
                            <h3>15+ პარტნიორი</h3>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- About Us Area End Here -->
<!-- Why Join Us Start Here -->
<section class="section-space-default bg-common bg-light" style="background-image: url(img/figure/figure3.png);">
    <div class="container">
        <div class="about-layout2">
            <div class="media align-items-center media-none--md">
                <div class="video-area overlay-primary80">
                    <img src="<?php bloginfo('template_url'); ?>/img/about/about1.jpg" alt="about" class="img-fluid width-100">
                    <a class="play-btn popup-youtube" href="https://www.youtube.com/watch?v=YcLVHHSHd10">
                        <img src="<?php bloginfo('template_url'); ?>/img/figure/play.png" alt="play" class="img-fluid">
                    </a>
                </div>
                <div class="media-body media-body-box">
                    <h3 class="title-bold color-dark size-lg margin-b-10">რატომ უნდა შემოგვიერთდე?</h3>
                    <p>დღეს ნებისმიერი ბიზნესისა თუ ინდივიდისთვის წარმატების მისაღწევად აუცილებელია ციფრულ ტექნოლოგიაზე ოპტიმიზირებულ სტრატეგიასა თუ სტრუქტურასთან ადაპტირება.</p><br>
                    <p>ციფრულ ეკოსისტემაზე მორგებულ, კომფორტულ გარემოში შეხვდები ადამიანებს, რომლებიც საკუთარ ხედვებსა და უნიკალურ მიდგომებს გაგიზიარებენ, რაც სასურველი კარიერული ეტაპების მიღწევაში დაგეხმარება.</p><br>
                    <div class="slider-btn-area forth-line">
                        <a href="http://digitaledu.ge/category/kursebi/" class="btn-fill color-blue border-radius-5 uptxt">კურსები</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Why Join Us Area End Here -->
<?php get_footer(); ?>

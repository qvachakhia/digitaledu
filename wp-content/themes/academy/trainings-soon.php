<?php /* Template Name: trainings */ ?>
<?php get_header(); ?>
<!-- Inne Page Banner Area Start Here -->
<section class="inner-page-banner" style="background-image: url(<?php bloginfo('template_url'); ?>/img/figure/inner-page-figure.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumbs-area">
                    <h2>ტრენინგები</h2>
                    <ul>
                        <li>
                            <a href="<?php echo home_url(); ?>">მთავარი</a>
                        </li>
                        <li>ტრენინგები</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inne Page Banner Area End Here -->
<!-- About Us Start Here -->
<section class="section-space-less30 bg-accent">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="center404">
                    <h3 class="title-black color-dark">მალე...</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- About Us Area End Here -->
<?php get_footer(); ?>

<form method="get" id="searchform" action="../">
	<input id="search" name="s" placeholder="" type="text" value="<?php the_search_query(); ?>" />
	<button id="searchsubmit" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
</form>
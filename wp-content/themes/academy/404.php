<?php /* Template Name: 404 */ ?>
<?php get_header(); ?>
<!-- Inne Page Banner Area Start Here -->
<section class="inner-page-banner" style="background-image: url(<?php bloginfo('template_url'); ?>/img/figure/inner-page-figure.jpg);">
</section>
<!-- Inne Page Banner Area End Here -->
<!-- Page Area Start Here -->
<section class="section-space-less30 bg-light center404">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
              <h1>404</h1>
              <p>გვერდი ვერ მოიძებნა</p>
              <div class="slider-btn-area forth-line">
                  <a href="<?php echo get_option('home'); ?>" class="btn-fill color-blue border-radius-5 uptxt">მთავარ გვერდზე დაბრუნება</a>
              </div>
            </div>
        </div>
    </div>
</section>
<!-- Page Area End Here -->


<?php get_footer(); ?>

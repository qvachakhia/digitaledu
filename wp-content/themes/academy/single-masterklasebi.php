<?php get_header(); ?>
<!-- Inne Page Banner Area Start Here -->
<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'main-post-large' );?>
<section class="inner-page-banner-s" style="background-image: url('<?php echo $thumb['0'];?>')">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumbs-area">
                  <h1><?php echo $cfs->get('masterklasis_dasaxeleba'); ?></h1>
                  <h2><?php the_title(); ?></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container mch-box">
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <div class="breadcrumbs-area mc-header mch-one">
                  <i class="fa fa-calendar" aria-hidden="true"></i>
                  <p class="uptxt"><?php echo $cfs->get('tarighi'); ?></p>
                  <p class="mc-header-txt"><?php echo $cfs->get('saati'); ?></p>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="breadcrumbs-area mc-header mch-two">
                  <i class="fa fa-map-marker" aria-hidden="true"></i>
                  <p class="uptxt"><?php echo $cfs->get('lokacia'); ?></p>
                  <p class="mc-header-txt"><?php echo $cfs->get('misamarti'); ?></p>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="breadcrumbs-area mc-header mch-three">
                  <p class="uptxt mc-buy-title">დაესწარი მასტერკლასს</p>
                  <p class="mc-price"><?php echo $cfs->get('fasi'); ?> <span class="lari">E</span></p>
                  <div class="slider-btn-area forth-line">
                      <a target="_blank" href="<?php echo $cfs->get('link'); ?>" class="btn-fill color-blue border-radius-5 uptxt">მასტერკლასის შეძენა</a>
                  </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inne Page Banner Area End Here -->
<!-- Page Area Start Here -->
<section class="section-space-less bg-dark">
    <div class="container mc-content-box">
      <?php if(have_posts()) : ?>
      <?php while(have_posts()) : the_post(); ?>
        <div class="row">
            <div class="col-md-12 col-sm-12 mc-content">
              <div class="videoWrapper"><?php echo $cfs->get('video'); ?></div>
              <?php the_content(); ?>
            </div>
        </div>
      <?php endwhile; ?>
      <?php endif; ?>
    </div>
</section>
<!-- Page Area End Here -->
<?php get_footer(); ?>

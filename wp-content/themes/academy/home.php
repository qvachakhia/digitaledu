<?php /* Template Name: home */ ?>
<?php get_header('home'); ?>

<!-- Slider Area Start Here -->
<div class="slider-direction-layout1 slider-layout1 ">
    <div class="bend niceties preview-1">
        <div id="ensign-nivoslider-1" class="slides">
          <img src="<?php bloginfo('template_url'); ?>/img/slider/graph-pro.png" alt="slider" title="#slider-direction-2" />
          <img src="<?php bloginfo('template_url'); ?>/img/slider/ui-ux.png" alt="slider" title="#slider-direction-3" />
        </div>
        
        <div id="slider-direction-2" class="t-cn slider-direction">
            <div class="slider-content s-tb slide-2">
                <div class="title-container s-tb-c title-light">
                    <div class="row">
            			      <div class="col-md-6 col-sm-12"></div>
                        <div class="col-md-6 col-sm-12">
                            <div class="container text-left">
                                <div class="first-line">
                                    <p class="sl-first-txt">ბრენდ დიზაინი</p>
                                </div>
                                <div class="second-line">
                                    <p class="uptxt sl-secend-txt">თაკო ჭაბუკიანი</p>
                                </div>
                                <div class="slider-sub-text third-line">
                                    <ul>
                                        <li>ბრენდ დიზაინის თეორია, ლოგოს კონსტრუქცია, ბადეები და კომპოზიცია,  ბრენდის იდენტობის ჩამოყალიბების პროცესი</li>
                                    </ul>
                                </div>
                                <div class="slider-btn-area forth-line">
                                    <a href="https://digitaledu.ge/brend-dizainis-kursi/" class="btn-fill color-blue border-radius-5 uptxt">კურსის ნახვა</a>
                                </div>
                            </div>
                    	</div>
                    </div>
                </div>
            </div>
        </div>
        <div id="slider-direction-3" class="t-cn slider-direction">
            <div class="slider-content s-tb slide-3">
                <div class="title-container s-tb-c title-light">
                    <div class="row">
            		        <div class="col-md-6 col-sm-12"></div>
                        <div class="col-md-6 col-sm-12">
                            <div class="container text-left">
                                <div class="first-line">
                                    <p class="sl-first-txt">UI & UX დიზაინი</p>
                                </div>
                                <div class="second-line">
                                    <p class="uptxt sl-secend-txt">ბაჩო ბარნაბიშვილი</p>
                                </div>
                                <div class="slider-sub-text third-line">
                                    <ul>
                                        <li>Ui/UX დიზაინის საფუძვლები, Adobe Experience design, SketchApp, ციფრული პროდუქტის დიზაინის შექმნა</li>
                                    </ul>
                                </div>
                                <div class="slider-btn-area forth-line">
                                    <a href="https://digitaledu.ge/ui-ux-kursi/" class="btn-fill color-blue border-radius-5 uptxt">კურსის ნახვა</a>
                                </div>
                            </div>
                    	</div>
                    </div>
                </div>
            </div>
        </div>
        

    </div>
</div>
<!-- Slider Area End Here -->
<!-- Masterckass Area Start Here -->
<section class="section-space-default-less30 bg-accent">
    <div class="container">
        <div class="section-heading title-dark color-dark text-center mw-title">
            <h3>მასტერკლასები</h3>
            <h4>პროფესიული მასტერკლასები ინდუსტრიის წამყვანი პროფესიონალებისგან</h4>
        </div>
        <div class="row">
            <?php query_posts('category_name=masterklasebi&showposts=6'); ?>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
              <div class="col-md-4 col-sm-12 mw-box">
                <p><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('main-post'); ?></a></p>
                <h1><a href="<?php the_permalink(); ?>"><?php echo $cfs->get('masterklasis_dasaxeleba'); ?></a></h1>
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="container">
      <div class="mw-border"></div>
      <div class="mw-more uptxt">
        <a href="http://digitaledu.ge/category/masterklasebi/">ყველა მასტერკლასის ნახვა</a>
      </div>
  </div>
</section>
<!-- Masterckass Area End Here -->
<!-- Trainings Area Start Here -->
<section class="section-space-default-less10 bg-accent tr-margin">
    <div class="container mt-block">
        <div class="section-heading title-dark color-dark text-center mw-title">
            <h3>კურსები</h3>
            <h4>პროფესიული კურსები ინდუსტრიის წამყვანი სპეციალისტებისგან</h4>
        </div>
        <div class="row">
            <?php query_posts('category_name=kursebi&showposts=14'); ?>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
              <div class="col-md-6 col-sm-12">
                <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
                <a href="<?php the_permalink(); ?>">
                  <div class="mt-box" style="background-image: url('<?php echo $thumb['0'];?>')">
                      <div class="container">
                        <div class="row">
                          <div class="col-md-8 col-8">
                            <h1><?php the_title(); ?></h1>
                            <p class="mt-box-tr-title uptxt">ტრენერი:</p>
                            <h3 class="mt-box-tr"><?php echo $cfs->get('pirveli_treneri'); ?></h3>
                            <h3 class="mt-box-tr"><?php echo $cfs->get('meore_treneri'); ?></h3>
                          </div>
                          <div class="col-md-4 col-4">
                          </div>
                        </div>
                      </div>
                  </div>
                </a>
              </div>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="container">
      <div class="mw-border"></div>
      <div class="mw-more uptxt">
        <a href="http://digitaledu.ge/category/kursebi/">ყველა კურსის ნახვა</a>
      </div>
  </div>
</section>
<?php get_footer(); ?>

<?php get_header(); ?>
<!-- Inne Page Banner Area Start Here -->
<section class="inner-page-banner" style="background-image: url(<?php bloginfo('template_url'); ?>/img/figure/inner-page-figure.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumbs-area">
                    <h2><?php foreach((get_the_category()) as $category) {$catname =$category->cat_name; echo $catname;} ?></h2>
                    <ul>
                        <li>
                            <a href="<?php echo home_url(); ?>">მთავარი</a>
                        </li>
                        <li><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?></li>
                    </ul>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inne Page Banner Area End Here -->

<!-- Area Start Here -->
<section class="section-space-default-less30 bg-accent">
    <div class="container zindex-up">
        <div class="row">
          <?php if(have_posts()) : ?>
    			<?php while(have_posts()) : the_post(); ?>
            <div class="col-md-4 col-sm-12 mw-box">
                <p><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('main-post'); ?></a></p>
                <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                <h2><a href="<?php the_permalink(); ?>"><?php echo $cfs->get('masterklasis_dasaxeleba'); ?></a></h2>
            </div>
          <?php endwhile; ?>
    			<?php endif; ?>
        </div>
    </div>
</section>
<!-- Area End Here -->
<?php get_footer(); ?>

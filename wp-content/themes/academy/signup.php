<?php /* Template Name: signup */ ?>
<?php get_header(); ?>
<section class="inner-page-banner" style="background-image: url(<?php bloginfo('template_url'); ?>/img/figure/inner-page-figure.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumbs-area">
                    <h2>რეგისტრაცია</h2>
                    <ul>
                        <li>
                            <a href="<?php echo home_url(); ?>">მთავარი</a>
                        </li>
                        <li>რეგისტრაცია</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<iframe src="https://survey.zohopublic.com/zs/6wCsYt" frameborder='0' style='height:1300px; width:100%;' marginwidth='0' marginheight='0' scrolling='0'></iframe>

<!-- Contact Form Area End Here -->
<?php get_footer(); ?>

<!doctype html>
<html class="no-js" lang="ka-GE">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php the_title(); ?> | ციფრული ინდუსტრიის აკადემია</title>
    <meta name="description" content="<?php echo (types_render_field( 'seodescription', array() )); ?>" />
    <meta name="keywords" content="<?php $posttags = get_the_tags(); if ($posttags) {foreach($posttags as $tag) { echo $tag->name . ', ';}}?> ">

    <meta property="fb:app_id" content="306135966643738" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php the_title(); ?>" />
    <meta property="og:description" content="ციფრული ინდუსტრიის აკადემია ..." />
    <meta property="og:url" content="<?php the_permalink(); ?>" />
    <meta property="og:image" content="<?php
        $thumb_id = get_post_thumbnail_id();
        $thumb_url = wp_get_attachment_image_src($thumb_id,'main-post', true);
        echo $thumb_url[0];
    ?>" />
    <meta property="og:image:alt" content="<?php the_title(); ?>" />

    <meta name="google-site-verification" content="s707xFGPApkTo9A8yr3jauHfPiHoCTP8QOyhnaNUNOY" />

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_url'); ?>/img/favicon.png">
    <!-- Normalize CSS -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/normalize.css">
    <!-- Main CSS -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/main.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/animate.min.css">
    <!-- Font-awesome CSS-->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/font-awesome.min.css">
    <!-- Main Menu CSS -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/meanmenu.min.css">
    <!-- Magnific CSS -->
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/magnific-popup.css">
    <link href="https://fonts.googleapis.com/css?family=Hind+Madurai:400,700" rel="stylesheet">
    <!-- nivo slider CSS -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/vendor/slider/css/nivo-slider.css" type="text/css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/vendor/slider/css/preview.css" type="text/css" media="screen">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <!-- Modernizr Js -->
    <script src="<?php bloginfo('template_url'); ?>/js/modernizr-2.8.3.min.js"></script>
    <?php wp_head(); ?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125964763-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-125964763-1');
    </script>
    <meta name="google-site-verification" content="AKh4iWNqFicMp7yEi_epz0ME3n2-LNzqf1XZWEClK-8" />
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TF6Q6Z2');</script>
    <!-- End Google Tag Manager -->
</head>

<body>

<div id="wrapper" class="wrapper">
<!-- Header Area Start Here -->
<header>
    <div id="header-one" class="header-area header-fixed full-width-compress acheader-border">
        <div class="main-menu-area" id="sticker">
            <div class="container-fluid">
                <div class="row no-gutters d-flex align-items-center">
                    <div class="col-lg-2 col-md-6 d-none d-lg-block">
                        <div class="logo-area">
                            <a href="<?php echo home_url(); ?>">
                                <img src="<?php bloginfo('template_url'); ?>/img/logo.png" alt="logo">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-6 possition-static">
                        <div class="eventalk-main-menu">
                            <nav class="d-none d-lg-block">
                                <?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container'=>false, 'menu_class'=> 'your_class_name' ) ); ?>
                            </nav>
                            <!-- Mobile Menu start -->
                            <nav id="dropdown" class="d-md-none">
                                <?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container'=>false, 'menu_class'=> 'your_class_name' ) ); ?>
                            </nav>
                            <!-- Mobile Menu End -->
                        </div>
                    </div>
                    <!--
                    <div class="col-lg-2 col-md-6 possition-static text-right signin">
                    	<a href="#"><i class="fa fa-user color-blue" aria-hidden="true"></i> <span class="uptxt color-blue">ავტორიზაცია</span></a>
                    </div>
                    -->
                    <div class="col-lg-2 col-md-6 possition-static text-right acsocial">
                        <ul>
                            <li><a target="_blank" href="https://www.facebook.com/digitaledu.ge"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                            <li><a target="_blank" href="https://www.instagram.com/digitaledu.ge/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a target="_blank" href="https://www.youtube.com/channel/UC4tqY_vBBa9N8EFnx9uv55Q"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                            <li><a target="_blank" href="https://www.linkedin.com/school/academyofdigitalindustries/"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</header>
<!-- Header Area End Here -->

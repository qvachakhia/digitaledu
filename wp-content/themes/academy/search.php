<?php get_header(); ?>
<div class="page-head">
    <div class="wrap">
		<h1>ბლოგი</h1>
        <ul>
        	<li><a href="<?php echo get_option('home'); ?>">მთავარი</a></li>
            <li><a href="<?php echo get_option('home'); ?>/blog/">ბლოგი</a></li>
        </ul>
        <div class="search-wrap">
            <div class="search">
                <?php get_search_form(); ?>
            </div>
        </div>
    </div>
</div>

<section class="section-wrapper">
	<div class="section">
        <div class="artcle-wrapper">
			<?php if(have_posts()) : ?>
            <?php while(have_posts()) : the_post(); ?>
                <article class="article">
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <!--<p class="author">ავტორი: <?php // the_author_posts_link() ?> </p>-->
                    <p class="date-and-cat">
                        <span class="post-date"><i class="fa fa-calendar" aria-hidden="true"></i>თარიღი <?php echo the_time('j. m. Y'); ?></span>
                        <span class="post-category"><i class="fa fa-folder-open-o" aria-hidden="true"></i>კატეგორია: <?php the_category(', '); ?></span>
                    </p>
                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('main-post'); ?></a>
                    <h3 class="post-content-index"><?php echo $cfs->get('short_description'); ?></h3>
                    <p class="post-more-button"><a href="<?php the_permalink(); ?>">სრულად წაკითხვა</a></p>
                </article>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_simple_pagination(); ?>
            <div class="mob-pagination"><p><?php posts_nav_link('&#8734;','&laquo; წინა გვერდი','შემდეგი გვერდი &raquo;'); ?></p></div>
        </div>
        <aside class="sidebar">
			<?php get_sidebar(); ?>
        </aside>
        <div class="clear"></div>
    </div>
</section>

<?php get_footer(); ?>
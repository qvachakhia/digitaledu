<?php /* Template Name: contact */ ?>
<?php get_header(); ?>
<!-- Inne Page Banner Area Start Here -->
<section class="inner-page-banner" style="background-image: url(<?php bloginfo('template_url'); ?>/img/figure/inner-page-figure.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumbs-area">
                    <h2>კონტაქტი</h2>
                    <ul>
                        <li>
                            <a href="<?php echo home_url(); ?>">მთავარი</a>
                        </li>
                        <li>კონტაქტი</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inne Page Banner Area End Here -->
<!-- Contact Form Area Start Here -->
<section class="section-space-default2-less30 bg-accent">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-12 margin-b-30rem">
				<h3 class="title-bold color-dark size-sm title-bar">მოგვწერეთ</h3>
				<form class="contact-form">
					<fieldset>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<input type="text" placeholder="სახელი და გვარი*" class="form-control" name="name" id="form-name" data-error="Name field is required"
										required>
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<input type="email" placeholder="ელ-ფოსტა*" class="form-control" name="email" id="form-email" data-error="Email field is required"
										required>
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<textarea placeholder="წერილი*" class="textarea form-control" name="message" id="form-message" rows="10" cols="20" data-error="Message field is required"
										required></textarea>
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="col-lg-4 col-md-6 col-sm-6 col-sm-12">
								<div class="form-group margin-b-none">
									<button type="submit" class="btn-fill color-blue border-radius-5 uptxt">გაგაზავნა</button>
								</div>
							</div>
							<div class="col-lg-8 col-md-6 col-sm-6 col-sm-12">
								<div class='form-response'></div>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
			<div class="sidebar-widget-area sidebar-break-md col-lg-4 col-md-12">
				<div class="widget">
					<h3 class="title-bold color-dark size-sm title-bar title-bar-high">კონტაქტი</h3>
					<div class="contact-us-info">
						<ul>
							<li>
								<i class="fa fa-map-marker" aria-hidden="true"></i>ი.ჭავჭავაძის 33ე, თბილისი. საქართველო</li>
							<li>
								<i class="fa fa-phone" aria-hidden="true"></i>+995 574 19 20 81</li>
							<li>
								<i class="fa fa-envelope-o" aria-hidden="true"></i>info@digitaledu.ge</li>
							<li>
								<i class="fa fa-link" aria-hidden="true"></i>www.digitaledu.ge</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Contact Form Area End Here -->
<?php get_footer(); ?>

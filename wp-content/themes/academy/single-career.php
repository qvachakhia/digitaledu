<?php get_header('career'); ?>
<!-- Inne Page Banner Area Start Here -->
<section class="inner-page-banner" style="background-image: url(<?php bloginfo('template_url'); ?>/img/figure/inner-page-figure-blog.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumbs-area">
                    <h2>ვაკანსია: <?php the_title(); ?></h2>
                    <ul>
                        <li>
                            <a href="<?php echo home_url(); ?>">მთავარი</a>
                        </li>
                        <li>კარიერა</li>
                    </ul>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inne Page Banner Area End Here -->

<div class="section-space-less30 bg-accent">
  <div class="container">
    <div class="row">
      <?php if(have_posts()) : ?>
      <?php while(have_posts()) : the_post(); ?>
      <div class="col-lg-12">
        <?php the_post_thumbnail('main-post-large'); ?>
        <div class="blog-content-box bg-light">
          <h3 class="blog-content-title"><?php the_title(); ?></h3>
          <p class="blog-date">გამოქვეყნების თარიღი: <span><?php echo (types_render_field( 'gamokveknebis-tarighi', array() )); ?></span> - ბოლო ვადა <span><?php echo (types_render_field( 'dedlaini', array() )); ?></span></p>
          <div class="blog-content">
            <?php the_content(); ?>
          </div>
        </div>
      </div>

      <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </div>
</div>

<?php get_footer(); ?>

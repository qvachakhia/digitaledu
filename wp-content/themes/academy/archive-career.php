<?php get_header(); ?>
<!-- Inne Page Banner Area Start Here -->
<section class="inner-page-banner" style="background-image: url(<?php bloginfo('template_url'); ?>/img/figure/inner-page-figure-career.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumbs-area">
                    <h2>კარიერა</h2>
                    <ul>
                        <li>
                            <a href="<?php echo home_url(); ?>">მთავარი</a>
                        </li>
                        <li>კარიერა</li>
                    </ul>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inne Page Banner Area End Here -->

<!-- Area Start Here -->
<section class="section-space-default-less30 bg-accent">
    <div class="container zindex-up">
        <div class="row">
            <div class="col-12 career">
              <div class="career-info">
                <p>პროექტ "კარიერის" ფარგლებში ვებ გვერდზე ატვირთული ნებისმიერი ვაკანსია სამსახურისა და დასაქმების შესახებ განკუთვნილია მხოლოდ აკადემიის კურსდამთავრებულებისთვის.</p>

                <p>პროექტის ფარგლებში მოძიებული ნებისმიერი  ვაკანსია სტუდენტს სთავაზობს ინტერნისა და ექსპეტის პოზიციებს და ამავდროულად კომპანიებს ეხმარება საჭირო კადრების მოძიებაში. პროფესიონალი კადრების მომზადებით ჩვენ ვქმნით სათანადო რესურსს შრომით ბაზარზე, რომელიც ორგანიზაციებს  რელევენტული და წარმატებული გადაწყვეტილებების მიღებაში ეხმარება.</p>

                <p>ჩვენი მიზანია, თითოეულ სტუდენტს მივცეთ პროფესიული განვითარების შესაძლებლობა, შევუქმნათ ინოვაციური გარემო და  გამორჩეულ  ურთიერთობებზე დაფუძნებული  კულტურა .თითოეული ვაკანსია სტუდენტს სთავაზობს საუკეთესო პირობებს, რომ ჰქონდეს სასურველი კარიერა ,რის საშუალებასაც აკადემიაში მიღებული სათანადო ცოდნა და ინოვაციური მიდგომები მისცემს</p>
              </div>
              <table class="table">
                <tr>
                  <th>პოზიციის დასახელება</th>
                  <th>გამოქვეყნების თარიღი</th>
                  <th>ბოლო ვადა</th>
                  <th class="rekomendirebuli-kursi">რეკომენდირებული კურსი</th>
                </tr>
                <?php if(have_posts()) : ?>
          			<?php while(have_posts()) : the_post(); ?>
                <tr>
                  <td><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></td>
                  <td><?php echo (types_render_field( 'gamokveknebis-tarighi', array() )); ?></td>
                  <td><?php echo (types_render_field( 'dedlaini', array() )); ?></td>
                  <td class="rekomendirebuli-kursi"><a target="_blank" href="<?php echo (types_render_field( 'rlink', array() )); ?>"><?php echo (types_render_field( 'rekomendirebuli-kursi', array() )); ?></a></td>
                </tr>
                <?php endwhile; ?>
                <?php endif; ?>
              </table>
            </div>
        </div>
    </div>
</section>
<section class="section-space-default-less30 bg-accent">
  <div class="container zindex-up">
    <div class="row">
      <?php if(function_exists('wp_paginate')) {wp_paginate();} ?>
    </div>
  </div>
</Section>
<!-- Area End Here -->
<?php get_footer(); ?>

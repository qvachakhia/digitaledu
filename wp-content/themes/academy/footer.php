<!-- Footer Area Start Here -->
<footer>
    <div class="footer-layout1 section-space-default-less54 bg-dark bg-primary">
        <div class="container zindex-up">
            <div class="row">
                <div class="col-12">
                    <a href="<?php echo home_url(); ?>" class="footer-logo">
                        <img src="<?php bloginfo('template_url'); ?>/img/about/footer-logo.png" alt="logo" class="img-fluid">
                    </a>
                    <p>© 2018. საქართველო, თბილისი | ყველა უფლება დაცულია</p>
                    <div class="footer-social">
                      <ul>
                          <li><a target="_blank" href="https://www.facebook.com/digitaledu.ge"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                          <li><a target="_blank" href="https://www.instagram.com/digitaledu.ge/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                          <li><a target="_blank" href="https://www.youtube.com/channel/UC4tqY_vBBa9N8EFnx9uv55Q"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                          <li><a target="_blank" href="https://www.linkedin.com/school/academyofdigitalindustries/"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                      </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer Area End Here -->
</div>
<!-- Wrapper End -->
<?php wp_footer();?>


<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      xfbml            : true,
      version          : 'v3.3'
    });
  };

  (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-mesijebi">
<div class="fb-customerchat"
  attribution=setup_tool
  page_id="223850724706670"
  logged_in_greeting="გამარჯობა! რით შემიძლია დაგეხმაროთ? 😊"
  logged_out_greeting="გამარჯობა! რით შემიძლია დაგეხმაროთ? 😊">
</div>
</div>



<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "CollegeOrUniversity",
  "name": "ციფრული ინდუსტრიის აკადემია",
  "alternateName": "Academy of Digital Industries",
  "url": "http://digitaledu.ge/",
  "logo": "http://digitaledu.ge/wp-content/themes/academy/img/logogeo.png",
  "contactPoint": {
    "@type": "ContactPoint",
    "telephone": "+995 577 52 32 55",
    "contactType": "customer service",
    "areaServed": "GE",
    "availableLanguage": "Georgian"
  },
  "sameAs": [
    "https://web.facebook.com/digitaledu.ge?_rdc=1&_rdr",
    "https://www.instagram.com/digitaledu.ge/",
    "https://www.youtube.com/channel/UC4tqY_vBBa9N8EFnx9uv55Q",
    "https://www.linkedin.com/school/academyofdigitalindustries/"
  ]
}
</script>

<!-- jquery-->
<script src="<?php bloginfo('template_url'); ?>/js/jquery-2.2.4.min.js"></script>
<!-- Plugins js -->
<script src="<?php bloginfo('template_url'); ?>/js/plugins.js"></script>
<!-- Popper js -->
<script src="<?php bloginfo('template_url'); ?>/js/popper.js"></script>
<!-- Bootstrap js -->
<script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>
<!-- Nivo slider js -->
<script src="<?php bloginfo('template_url'); ?>/vendor/slider/js/jquery.nivo.slider.js"></script>
<script src="<?php bloginfo('template_url'); ?>/vendor/slider/home.js"></script>
<!-- Meanmenu Js -->
<script src="<?php bloginfo('template_url'); ?>/js/jquery.meanmenu.min.js"></script>
<!-- Srollup js -->
<script src="<?php bloginfo('template_url'); ?>/js/jquery.scrollUp.min.js"></script>
<!-- jquery.counterup js -->
<script src="<?php bloginfo('template_url'); ?>/js/jquery.counterup.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/waypoints.min.js"></script>
<!-- Countdown js -->
<script src="<?php bloginfo('template_url'); ?>/js/jquery.countdown.min.js"></script>
<!-- Isotope js -->
<script src="<?php bloginfo('template_url'); ?>/js/isotope.pkgd.min.js"></script>
<!-- Google Map js -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgREM8KO8hjfbOC0R_btBhQsEQsnpzFGQ"></script>
<!-- Magnific Popup -->
<script src="<?php bloginfo('template_url'); ?>/js/jquery.magnific-popup.min.js"></script>
<!-- Custom Js -->
<script src="<?php bloginfo('template_url'); ?>/js/main.js"></script>
</body>
</html>

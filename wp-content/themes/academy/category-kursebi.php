<?php get_header('kursebi'); ?>
<!-- Inne Page Banner Area Start Here -->
<section class="inner-page-banner" style="background-image: url(<?php bloginfo('template_url'); ?>/img/figure/inner-page-figure.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumbs-area">
                    <h2>კურსები</h2>
                    <ul>
                        <li>
                            <a href="<?php echo home_url(); ?>">მთავარი</a>
                        </li>
                        <li>კურსები</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inne Page Banner Area End Here -->

<!-- Area Start Here -->
<section class="section-space-default-less30 bg-accent">
    <div class="container zindex-up">
        <div class="row">
          <?php if(have_posts()) : ?>
    			<?php while(have_posts()) : the_post(); ?>
            <div class="col-md-6 col-sm-12">
              <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
              <a href="<?php the_permalink(); ?>">
                <div class="mt-box" style="background-image: url('<?php echo $thumb['0'];?>')">
                    <div class="container">
                      <div class="row">
                        <div class="col-md-8 col-8">
                          <h1><?php the_title(); ?></h1>
                          <p class="mt-box-tr-title uptxt">ტრენერი:</p>
                          <h3 class="mt-box-tr"><?php echo $cfs->get('pirveli_treneri'); ?></h3>
                          <h3 class="mt-box-tr"><?php echo $cfs->get('meore_treneri'); ?></h3>
                        </div>
                        <div class="col-md-4 col-4">
                        </div>
                      </div>
                    </div>
                </div>
              </a>
            </div>
          <?php endwhile; ?>
    			<?php endif; ?>
        </div>
    </div>
</section>
<!-- Area End Here -->
<?php get_footer(); ?>
